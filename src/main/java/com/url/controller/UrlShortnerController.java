package com.url.controller;


import com.url.model.UrlRequest;
import com.url.model.UrlResponse;
import com.url.service.LinkService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class UrlShortnerController {

    private final LinkService linkService;

    @PostMapping("/link")
    public Mono<UrlResponse> getShortUrl(UrlRequest url){
        return linkService.shortenLink(url.getOriginalLink()).map(UrlResponse::new);
    }
}
