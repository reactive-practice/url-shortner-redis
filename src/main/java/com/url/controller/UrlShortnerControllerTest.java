package com.url.controller;

import com.url.controller.UrlShortnerController;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = UrlShortnerController.class)
public class UrlShortnerControllerTest {

    @Autowired
    WebTestClient webTestClient;


    @Test
    public void shortensLink(){
        webTestClient.post()
                .uri("/link")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("{\"link\":\"www.spring.io\"}")
                .exchange()
                .expectStatus()
                .is2xxSuccessful();

    }



}
