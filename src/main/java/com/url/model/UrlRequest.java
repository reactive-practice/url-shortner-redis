package com.url.model;

import lombok.Value;

@Value
public class UrlRequest {

    private String originalLink;
}
