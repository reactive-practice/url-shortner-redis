package com.url.model;

import lombok.Value;

@Value
public class UrlResponse {
    private String shortenLink;
}
