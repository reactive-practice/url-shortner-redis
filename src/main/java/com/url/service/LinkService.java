package com.url.service;


import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import org.apache.commons.lang3.RandomStringUtils;

@Service
public class LinkService {

    public Mono<String> shortenLink(String originalLink){
        String shortLinkKey = RandomStringUtils.randomAlphanumeric(6);
        return Mono.just("http://localhost:8080/"+shortLinkKey);
    }


}
